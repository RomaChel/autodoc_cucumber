package page_object_for_site;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CartAllDataPage {

    SelenideElement nextBtn() {
        return $(byCssSelector(".order-summary__button"));
    }

    // Locators for bestelen-block (Safe order)
    SelenideElement safeOrderCheckbox() {
        return $x("//input[@name='security_delivery']");
    }
    public SelenideElement deliveryPrice() {
        return $(byXpath("(//div[@class='order-summary__row']/span[@class='order-delivery'])[2]"));
    }

    public SelenideElement totalOrderPrice() {
        return $(byXpath("//*[@class='alldata-bottom']//*[contains(@class,'total')]/span[2]"));
    }

    SelenideElement labelVAT() {
        return $x("//*[@class='alldata-bottom']//*[contains(@class,'total')]//i");
    }
}
