package page_object_for_site;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.visible;
import static java.time.Duration.ofSeconds;

public class PaymentHandlerPageLogic extends PaymentHandlerPage {

    public PaymentHandlerPageLogic checkSuccessTextInHeader() {
        successTextInHeader().shouldBe(appear, ofSeconds(20));
        return this;
    }

    public String getOrderNumber() {
        closePopupAfterOrder();
        return orderNumber().getText();
    }

    public PaymentHandlerPageLogic closePopupAfterOrder() {
        try {
            popupAfterOrder().shouldBe(visible, ofSeconds(10));
            closePopupAfterOrderBtn().click();
        } catch (Throwable e) {
            System.out.println("Popup not visible");
        }
        return this;
    }
}
