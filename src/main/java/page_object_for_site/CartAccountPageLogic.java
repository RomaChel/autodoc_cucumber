package page_object_for_site;

import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static common.CommonMethods.clickByJS;
import static java.time.Duration.ofSeconds;


public class CartAccountPageLogic extends CartAccountPage {

    public CartAddressPageLogic signIn(String email, String password) {
        emailFieldInLoginForm().shouldBe(visible, ofSeconds(20));
        emailFieldInLoginForm().setValue(email);
        passwordFieldInLoginForm().setValue(password);
        emailFieldInLoginForm().shouldHave(value(email));
        passwordFieldInLoginForm().shouldHave(value(password));
        clickByJS(loginButton());
        if (errorPopUpWhenLogin().isDisplayed()) {
            closeErrorPopUpBtn().shouldBe(visible).click();
            emailFieldInLoginForm().shouldBe(visible).setValue(email);
            passwordFieldInLoginForm().setValue(password);
            emailFieldInLoginForm().shouldHave(value(email));
            passwordFieldInLoginForm().shouldHave(value(password));
            loginButton().shouldBe(visible, ofSeconds(10));
            clickByJS(loginButton());
        }
        new CartAddressPage().shippingForm().shouldBe(visible, ofSeconds(30));
        return page(CartAddressPageLogic.class);
    }
}
