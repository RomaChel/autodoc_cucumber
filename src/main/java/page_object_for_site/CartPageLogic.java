package page_object_for_site;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static java.time.Duration.ofSeconds;

public class CartPageLogic extends CartPage {

    public CartAccountPageLogic nextButtonClick() {
        nextButton().shouldBe(visible, ofSeconds(10)).click();
        return page(CartAccountPageLogic.class);
    }
}
