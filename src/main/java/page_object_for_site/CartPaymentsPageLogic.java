package page_object_for_site;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static common.CommonMethods.clickByJS;
import static java.time.Duration.ofSeconds;

public class CartPaymentsPageLogic extends CartPaymentsPage {

    public CartPaymentsPageLogic chooseVorkasse() {
        vorkasseBtn().shouldBe(visible, ofSeconds(10)).click();
        return this;
    }

    public CartAllDataPageLogic nextBtnClick() {
        clickByJS(nextBtn());
        paymentsList().shouldNotBe(visible, ofSeconds(15));
        return page(CartAllDataPageLogic.class);
    }
}
