package page_object_for_site;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;
import static common.CommonMethods.getCurrentShopFromJSVarInHTML;
import static java.time.Duration.ofSeconds;

public class CartAddressPageLogic extends CartAddressPage{

    public CartAddressPageLogic fillAllFieldsForShipping(String shop) {
        checkCorrectTextAndFillInput(fieldName(), "autotest");
        checkCorrectTextAndFillInput(fieldLastName(), "autotest");
        checkCorrectTextAndFillInput(fieldStreet(), "autotest");
        checkCorrectTextAndFillInput(fieldHouse(), "autotest");
        chooseDeliveryCountryForShipping(shop);
        fillInPostalCode("default");
        checkCorrectTextAndFillInput(fieldCity(), "autotest");
        checkCorrectTextAndFillInput(fieldTelephoneShipping(), "200+002");
        return this;
    }

    public CartAddressPageLogic chooseDeliveryCountryForShipping(String country) {
        shippingForm().shouldBe(appear, ofSeconds(10));
        if (country.equals("EN")) country = "GB";
        if (country.equals("LD")) country = "LU";
        if (country.equals("BE_FR")) country = "BE";
        countryInSelectorForShipping(country).shouldBe(visible).click();
        return this;
    }

    public void checkCorrectTextAndFillInput(SelenideElement element, String correctText) {
        Configuration.fastSetValue = false;
        if (!element.getValue().equals(correctText)) {
            element.clear();
            element.setValue(correctText);
        }
    }

    public CartAddressPageLogic fillInPostalCode(String postalCodeOrCodeDefault) {
        if (postalCodeOrCodeDefault.equals("default")) {
            String currentShop = getCurrentShopFromJSVarInHTML();
            switch (currentShop) {
                case "DK":
                    postalCodeOrCodeDefault = "1234";
                    break;
                case "NL":
                    postalCodeOrCodeDefault = "1234 AA";
                    break;
                case "PT":
                    postalCodeOrCodeDefault = "1234-567";
                    break;
                default:
                    postalCodeOrCodeDefault = "12345";
                    break;
            }
        }
        fieldPostalCodeForShipping().clear();
        fieldPostalCodeForShipping().click();
        checkCorrectTextAndFillInput(fieldPostalCodeForShipping(), postalCodeOrCodeDefault);
        return this;
    }

    public CartPaymentsPageLogic nextBtnClick() {
        nextButton().shouldBe(visible, ofSeconds(5));
        nextButton().click();
        sleep(5000);
        return page(CartPaymentsPageLogic.class);
    }
}
