package page_object_for_site;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    SelenideElement nextButton() {
        return $(byCssSelector(".next-step"));
    }
}
