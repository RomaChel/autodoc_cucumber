package page_object_for_site;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CartAccountPage {

    // locators for loginForm (login)
    public SelenideElement emailFieldInLoginForm() {
        return $(byXpath("(//input[@id='form_Email'])[1]"));
    }
    public SelenideElement passwordFieldInLoginForm() {
        return $(byXpath("(//input[@id='form_Password'])[1]"));
    }
    public SelenideElement loginButton() {
        return $(byCssSelector(".login"));
    }
    SelenideElement errorPopUpWhenLogin() {
        return $x("//div[contains(@class,'cart-popup')]");
    }
    public SelenideElement closeErrorPopUpBtn() {
        return $x("//a[@class='color close_popup']");
    }
}
