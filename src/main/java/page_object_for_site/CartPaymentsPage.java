package page_object_for_site;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CartPaymentsPage {

    SelenideElement vorkasseBtn() {
        return $(byId("hypovereinsbank"));
    }

    // Next step button
    SelenideElement nextBtn() {
        return $(byCssSelector("[id='apply_payment']>a"));
    }

    // locators for payment types
    SelenideElement paymentsList() {
        return $x("//div[@class='payments-list']");
    }
}
