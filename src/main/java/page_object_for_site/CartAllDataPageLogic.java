package page_object_for_site;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.sleep;
import static common.CommonMethods.*;

public class CartAllDataPageLogic extends CartAllDataPage {

    public CartAllDataPageLogic checkRegularDeliveryPrice(float regularDeliveryPrice) {
        Float deliveryPrice = getRegularDeliveryPrice();
        float res = roundOfTheCost(regularDeliveryPrice, deliveryPrice);
        deliveryPrice().shouldHave(text(String.valueOf(res).replaceAll("\\.", ",")));
        return this;
    }

    public float getRegularDeliveryPrice() {
        String deliveryPrice = deliveryPrice().getText();
        return Float.parseFloat(deliveryPrice.replaceAll("[^\\d,]", "").replaceAll(",","."));
    }

    public CartAllDataPageLogic clickSafeOrderCheckbox() {
        safeOrderCheckbox().click();
        sleep(3000); // TODO из за дефекта BSK-1226
        return this;
    }

    public PaymentHandlerPageLogic nextBtnClick() {
        clickByJS(nextBtn());
        new PaymentHandlerPageLogic().checkSuccessTextInHeader();
        checkingContainsUrl("/bank/success");
        return page(PaymentHandlerPageLogic.class);
    }

    public Float getTotalPriceAllDataPage(String shop) {
        Float totalPrice;
        totalOrderPrice().shouldBe(visible);
        if (shop.equals("EN")) {
            totalPrice = getTotalPriceAllDataPageForEnShop();
        } else {
            String realPrice = totalOrderPrice().getText();
            realPrice = realPrice.substring(0, realPrice.indexOf(" ")).replaceAll(",", ".");
            totalPrice = Float.parseFloat(realPrice);
        }
        return totalPrice;
    }

    public Float getTotalPriceAllDataPageForEnShop() {
        String realPrice;
        if (labelVAT().isDisplayed()) {
            String vat = labelVAT().getText();
            realPrice = totalOrderPrice().getText().replace("£ ", "").replace(vat, "");
        } else {
            realPrice = totalOrderPrice().getText().replace("£ ", "");
        }
        realPrice = realPrice.replaceAll(",", ".");
        return Float.parseFloat(realPrice);
    }
}
