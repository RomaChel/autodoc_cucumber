package page_object_for_site;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CartAddressPage {

    public SelenideElement shippingForm() {
        return $x("//form[@id='shipping_form']");
    }

    public SelenideElement fieldName() {
        return $(byId("form_lVorname"));
    }

    public SelenideElement fieldLastName() {
        return $(byId("form_lName"));
    }

    public SelenideElement fieldStreet() {
        return $(byId("form_lStrasse"));
    }

    SelenideElement fieldHouse() {
        return $(byId("form_delivery_house"));
    }

    public SelenideElement fieldPostalCodeForShipping() {
        return $(By.id("form_lPlz"));
    }

    public SelenideElement fieldCity() {
        return $(byId("form_lOrt"));
    }

    SelenideElement countryInSelectorForShipping(String country) {
        return $(byXpath("//*[@name='lLand']//*[@data-code='" + country + "']"));
    }

    SelenideElement fieldTelephoneShipping() {
        return $(byId("form_lTelefon"));
    }

    SelenideElement telephoneCodeSelector(String country) {
        return $x("//select[@name='lTelefonCode']//*[@data-code='" + country + "']");
    }

    // Locator for next page button
    public SelenideElement nextButton() {
        return $(byCssSelector(".address-continue>a"));
    }
}
