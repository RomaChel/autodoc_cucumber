package page_object_for_site;

import com.codeborne.selenide.ex.ElementNotFound;
import com.codeborne.selenide.ex.ElementShould;
import com.codeborne.selenide.ex.UIAssertionError;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static common.CommonMethods.clickByJS;
import static common.CommonMethods.closeQuestionsPopupIfYes;
import static java.time.Duration.ofSeconds;

public class ProductPageLogic extends ProductPage {

    public CartPageLogic cartClick() {
        clickByJS(cartIcon().shouldBe(appear, ofSeconds(10)));
        return page(CartPageLogic.class);
    }

    public ProductPageLogic checkNumberBasketAndRefreshPageIfNot() {
        // TODO Бывает при открытии страницы не подгружается номер корзины и товар не добавляется в корзину,
        //  причина не известна, что бы стабилизировать тесты добавлен этот метод
        try {
            numberBasket().scrollTo().shouldBe(visible);
        } catch (ElementShould e) {
            refresh();
            numberBasket().scrollTo().shouldBe(visible);
        }
        return this;
    }

    public ProductPageLogic checksPresentProductInCartPopup() {
        cartIcon().scrollIntoView("{block: \"center\"}").shouldBe(visible, ofSeconds(10)).hover();
        firstProductPriceInPopupOfCart().shouldBe(visible, ofSeconds(5));
        return this;
    }

    public ProductPageLogic closePopupOtherCategoryIfYes() {
        try {
            closeBtnOfPopupOtherCategory().shouldBe(appear, ofSeconds(5));
            closeBtnOfPopupOtherCategory().click();
            closeBtnOfPopupOtherCategory().shouldNotBe(appear, ofSeconds(3));
        } catch (ElementNotFound ignored) {
        }
        return this;
    }

    public ProductPageLogic addProductToCart() {
        checkNumberBasketAndRefreshPageIfNot();
        sleep(3000); // TODO для стабилизации. Без слипа иногда добавленный товар исчезает из корзины после перехода в неё, решается в SITES-2830
        closeQuestionsPopupIfYes();
        buyButton().shouldBe(appear, ofSeconds(10));
        clickByJS(buyButton());
        closePopupOtherCategoryIfYes();
        try {
            checksPresentProductInCartPopup();
        } catch (UIAssertionError e) {
            closePopupOtherCategoryIfYes();
            buyButton().click();
            checksPresentProductInCartPopup();
        }
        return this;
    }
}
