package page_object_for_site;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selenide.$;

public class ProductPage {

    SelenideElement numberBasket() {
        return $(byCssSelector(".code"));
    }
    public SelenideElement buyButton() {
        return $(byCssSelector(".product-button>a"));
    }
    public SelenideElement cartIcon() {
        return $(byCssSelector(".header-cart__count"));
    }
    // locators in popup of cart
    public SelenideElement firstProductPriceInPopupOfCart() {
        return $(byCssSelector(".row-price"));
    }
    SelenideElement closeBtnOfPopupOtherCategory() {
        return $(byCssSelector(".popup-other-cat__close"));
    }
}
