package common;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.pageLoadTimeout;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static java.time.Duration.ofSeconds;

public class CommonMethods {

    public static SelenideElement popUpQuestions() {
        return $x("//*[@class='popup-after-order']");
    }
    public static SelenideElement popUpQuestionsCloseButton() {
        return $x("//*[@class='popup-after-order__close']");
    }

    public static void closeQuestionsPopupIfYes() {
        try {
            popUpQuestions().shouldBe(visible, ofSeconds(5));
            popUpQuestionsCloseButton().click();
            popUpQuestions().shouldBe(not(visible));
        } catch (UIAssertionError ignored) {
            System.out.println("Pop-up is not found");
        }
    }

    public static void clickByJS(SelenideElement element) {
        JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
        js.executeScript("arguments[0].click();", element);
    }

    public static void openPage(String url) {
        System.out.println(url);
        open(url);
    }

    public static String getCurrentUrl() {
        return url();
    }

    public static String getCurrentShopFromJSVarInHTML() {
        String currentShop = executeJavaScript("return $siteSettings.lang");
        if (currentShop.equals("lu")) currentShop = "ld";
        if (currentShop.equals("be")) {
            String url = getCurrentUrl();
            if (url.contains("auto-doc.be")) currentShop = "BE_FR";
        }
        return currentShop.toUpperCase();
    }

    public static Float roundOfTheCost(Float cost, Float expectedCost) {
        BigDecimal result = new BigDecimal(cost);
        BigDecimal formatCostUp = result.setScale(2, RoundingMode.UP);
        float roundMax = Float.parseFloat(String.valueOf(formatCostUp));
        BigDecimal formatCostDown = result.setScale(2, RoundingMode.FLOOR);
        float roundMin = Float.parseFloat(String.valueOf((formatCostDown)));
        float res = 0.0f;
        if (expectedCost.equals(roundMax)) {
            return res = roundMax;
        } else {
            BigDecimal resultAfter = new BigDecimal(roundMax);
            BigDecimal costUP = resultAfter.setScale(2, RoundingMode.UP);
            float formatCostUP = Float.parseFloat(String.valueOf(costUP));
            if (expectedCost.equals(formatCostUP)) {
                return res = formatCostUP;
            }
        }
        if (expectedCost.equals(roundMin)) {
            return res = roundMin;
        } else {
            BigDecimal resultAfter = new BigDecimal(roundMin);
            BigDecimal costDOWN = resultAfter.setScale(2, RoundingMode.DOWN);
            float formatCostDOWN = Float.parseFloat(String.valueOf(costDOWN));
            if (expectedCost.equals(formatCostDOWN)) {
                return res = formatCostDOWN;
            }
            return res;
        }
    }

    public static void checkingContainsUrl(String expectedContainsUrl) {
        pageLoadTimeout = 200000;
        try {
            Wait().until(webDriver -> url().contains(expectedContainsUrl));
        } catch (TimeoutException e) {
            System.out.println(url());
            Assert.fail("Url doesn't contains: " + expectedContainsUrl);
        }
    }
}
