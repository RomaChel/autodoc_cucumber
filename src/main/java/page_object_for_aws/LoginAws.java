package page_object_for_aws;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static java.time.Duration.ofSeconds;

public class LoginAws {

    public static final String EMAIL = "autodoc-6731";
    public static final String PASSWORD = "q^rkmQ7w";

    public static SelenideElement loginField() {
        return $(byId("login"));
    }

    private SelenideElement passwordField() {
        return $(byId("password"));
    }

    private SelenideElement loginButton() {
        return $(byXpath("//button[@class='btn btn-default btn-sm pull-right']"));
    }

    public MainAws loginInAws() {
        loginField().setValue(EMAIL);
        passwordField().setValue(PASSWORD);
        loginButton().shouldBe(visible,ofSeconds(10));
        loginButton().click();
        loginButton().shouldNotBe(visible, ofSeconds(35));
        return page(MainAws.class);
    }
}
