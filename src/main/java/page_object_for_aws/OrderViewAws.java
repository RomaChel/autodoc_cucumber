package page_object_for_aws;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;

public class OrderViewAws {

    private String orderNumber;
    private String url = "https://aws.autodoc.de/order/view/";

    public OrderViewAws(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OrderViewAws() {
    }

    private SelenideElement phoneNumberField() {
        return $(byName("Order[rTelefon]"));
    }

    private SelenideElement testIcon() {
        return $("[data-hint='Test']");
    }

    private SelenideElement totalPriceOrder() {
        return $x("//td[@class='inf_grandTotal']");
    }

    private SelenideElement deliveryPriceOrderAWS() {
        return $(".inf_deliveryCost > a");
    }

    private SelenideElement safeOrderSelector() {
        return $(byId("form_securityDeliveryStatusChange"));
    }

    private SelenideElement btnChangeOrderStatusInTest() {
        return $x("//button[@class='btn btn-primary']");
    }

    private SelenideElement saveChangesInOrderBtn() {
        return $x("//button[@class='btn btn-success submit-order']");
    }


    public OrderViewAws openOrderInAwsWithLogin() {
        open(url + orderNumber);
        new LoginAws().loginInAws();
        checkWhatOrderOpened();
        checkOrderHasTestPhone();
        testIcon().shouldBe(visible);
        return this;
    }

    private OrderViewAws checkWhatOrderOpened() {
        // Иногда, если заказ в AWS открыть сразу быстро после создания, он может не успеть подгрузися в AWS
        if ($("body").text().equals("Not found")) {
            refresh();
            phoneNumberField().shouldBe(visible);
        }
        return this;
    }

    public OrderViewAws checkOrderHasTestPhone() {
        phoneNumberField().shouldHave(or("value", value("+002"), value("+001")), ofSeconds(60));
        testIcon().shouldBe(appear, ofSeconds(10));
        return this;
    }

    public Float getTotalPriceOrderAWS() {
        return Float.valueOf(totalPriceOrder().getText());
    }

    public OrderViewAws checkDeliveryPriceOrderAWS(Float expectedDeliveryPriceOrderAWS) {
        deliveryPriceOrderAWS().shouldHave(attribute("data-sum", String.valueOf(expectedDeliveryPriceOrderAWS)));
        return this;
    }

    public OrderViewAws checkThatStatusSafeOrderIsOn() {
        safeOrderSelector().shouldHave(text("Включен"));
        return this;
    }

    public OrderViewAws reSaveOrder() {
        btnChangeOrderStatusInTest().scrollIntoView("{block: \"center\"}");
        btnChangeOrderStatusInTest().click();
        saveChangesInOrderBtn().shouldBe(appear, ofSeconds(15)).click();
        saveChangesInOrderBtn().shouldBe(appear, ofSeconds(50));
        return this;
    }
}
