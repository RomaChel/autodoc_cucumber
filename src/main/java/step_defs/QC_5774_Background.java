package step_defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import page_object_for_site.CartAccountPageLogic;
import page_object_for_site.ProductPageLogic;

import static common.CommonMethods.openPage;

public class QC_5774_Background {

    private ProductPageLogic productPageLogic = new ProductPageLogic();


    @Given("the page opened on uri {string}")
    public void the_page_opened_on_uri(String string) {
        openPage(string);
    }

    @Given("added drop product to basket")
    public void added_drop_product_to_basket() {
        //System.out.println("HERE");
        productPageLogic.addProductToCart();
    }

    @Given("added simple product to basket")
    public void added_simple_product_to_basket() {
        //System.out.println("HERE");
        productPageLogic.addProductToCart();
    }

    @When("go to basket")
    public void go_to_basket() {
        //System.out.println("HERE");
        productPageLogic.cartClick()
                .nextButtonClick(); // данный шаг должен быть промежуточным
    }

    @When("Login")
    public void login() {
        //System.out.println("HERE");
        new CartAccountPageLogic().signIn("qc_6090_autotestDE@mailinator.com", "atdtest");
    }

}
