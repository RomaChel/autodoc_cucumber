package step_defs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import page_object_for_aws.OrderViewAws;
import page_object_for_site.CartAddressPageLogic;
import page_object_for_site.CartAllDataPageLogic;
import page_object_for_site.CartPaymentsPageLogic;
import page_object_for_site.PaymentHandlerPageLogic;

public class QC_5774_Steps {

    private CartAddressPageLogic cartAddressPageLogic = new CartAddressPageLogic();
    private CartPaymentsPageLogic cartPaymentsPageLogic = new CartPaymentsPageLogic();
    private CartAllDataPageLogic cartAllDataPageLogic = new CartAllDataPageLogic();
    private OrderViewAws orderViewAws = new OrderViewAws();

    @When("on page {string} choose delivery country like shop")
    public void on_page_choose_delivery_country_like_shop(String string) {
        cartAddressPageLogic.fillAllFieldsForShipping(string);
    }

    @When("click on the next button")
    public void click_on_the_next_button() {
        //System.out.println("HERE");
        cartAddressPageLogic.nextBtnClick();
    }

    @When("on page {string} select bank as a payment method")
    public void on_page_select_bank_as_a_payment_method(String string) {
        //System.out.println("HERE");
        cartPaymentsPageLogic.chooseVorkasse();
    }

    @Then("opens page {string}")
    public void opens_page(String string) {
        //System.out.println("HERE");
        cartPaymentsPageLogic.nextBtnClick();
    }

    @Then("should be visible the standard shipping")
    public void should_be_visible_the_standard_shipping() {
        //System.out.println("HERE");
        cartAllDataPageLogic.checkRegularDeliveryPrice(19.50f); // TODO в реальном проекте данную цену
                                                                // TODO мы достаем перед началом самого теста c страницы Versand
    }

    @Then("save the sum of the order")
    public Float save_the_sum_of_the_order() {
        //System.out.println("HERE");
        return cartAllDataPageLogic.getTotalPriceAllDataPage("DE");
    }

    @When("click on the SO checkbox")
    public void click_on_the_so_checkbox() {
        //System.out.println("HERE");
        cartAllDataPageLogic.clickSafeOrderCheckbox();
    }

    @When("click on the buy button")
    public void click_on_the_buy_button() {
        //System.out.println("HERE");
        cartAllDataPageLogic.nextBtnClick();
    }

    @When("opens the page {string} save the order number")
    public String opens_the_page_save_the_order_number(String string) { // TODO тут ненужен никаой стринг
        //System.out.println("HERE");
        String orderNumber = new PaymentHandlerPageLogic().getOrderNumber();
        return orderNumber;
    }

    @When("open URL {string}")
    public void open_url(String string) {
        //System.out.println("HERE");
        OrderViewAws orderViewAws = new OrderViewAws(opens_the_page_save_the_order_number(string));
        orderViewAws.openOrderInAwsWithLogin();
    }

    @When("search created order by id and open it")
    public void search_created_order_by_id_and_open_it() {
        System.out.println("HERE");
    } // TODO я не понимаю что это за шаг и что он должен сделать

    @Then("the sum of the order should be equal to the sum from the alldata page")
    public void the_sum_of_the_order_should_be_equal_to_the_sum_from_the_alldata_page() {
        //System.out.println("HERE");
        Assert.assertEquals(orderViewAws.getTotalPriceOrderAWS(), save_the_sum_of_the_order());
    }

    @Then("should be visible standard shipping")
    public void should_be_visible_standard_shipping() {
        //System.out.println("HERE");
        orderViewAws.checkDeliveryPriceOrderAWS(19.50f); //TODO это предроложительная цена, нужно сотреть в реали скольку
                                                                                           //TODO ну и у нас она еще прибавляет SO туту нету этого условия
    }

    @Then("the SO should be turned on")
    public void the_so_should_be_turned_on() {
        //System.out.println("HERE");
        orderViewAws.checkThatStatusSafeOrderIsOn();
    }

    @When("Re-save order")
    public void re_save_order() {
        //System.out.println("HERE");
        orderViewAws.reSaveOrder();
    }

    @Then("the data in order shouldn't be changed")
    public void the_data_in_order_shouldn_t_be_changed() { // TODO я хз как этот шаг реализовать но как вариант то вот так если так можно:
        //System.out.println("HERE");
        should_be_visible_standard_shipping();
        the_so_should_be_turned_on();
        the_sum_of_the_order_should_be_equal_to_the_sum_from_the_alldata_page();
    }

}
